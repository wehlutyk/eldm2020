Checklist for streaming ELDM
============================

Before the event
----------------

Describe the setup on the website:
- Youtube Live (no comments) + Jitsi Meet (with registration)
- Oral questions on Jitsi, written questions on Twitter #eldm2020

Youtube:
- Schedule a Youtube Live stream and save its stream key.
- Set a description with the website, programme, probable downtimes
- Set the stream url in the embedded view in ELDM's website.

Mastodon/Twitter:
- Publicise the #eldm2020 hashtag as the main channel for remote written questions

In the morning, a few hours before the start of the workshop
------------------------------------------------------------

Wiring for keon:
- Log in to Gnome Shell
- Use an ethernet cable to the network and deactivate wifi
- Plug the sound output into the room's speakers, or a small local speaker
- Plug in the videoprojector, deactivate Gnome's screen dimming and blanking when inactive

D8.001 video setup:
- Turn on control computer
- Turn on mixer and camera control board
- In control computer:
  - Go to 'Enregistrer', check we have video and sound
  - Then 'Retour', then 'Paramètres Streaming' to chetk the RTMP url and rendering settings
  - Then 'Retour', then 'Accueil', and back to 'Enregistrer'
  - Click 'Live' to start streaming and recording

Jitsi:
- Get the `rtmp` url for the room's cameras and audio from ENS Media, and give that to `./rtmp-ffmpeg-v4l2.sh <rtmp-url>`.
- Create a Jitsi Meet room named `ELDM2020-randomstring`, with a password
- Have it use the rtmp video and audio
- Move the window to the videoprojector screen
- Set the 'Everyone starts muted' option
- Keep this room open so I remain moderator
- Connect with my phone to check it works, talking in both directions

Mail to Jitsi callers who registered, a couple hours before the workshop starts, with:
- Call link and password
- Instructions: use a headset, raise hand to ask a question, mute yourself when not talking, if you see no one we're on a break/food
- Please refer to programme for uptime/downtime

Start Youtube streaming:
- Open OBS and give it the stream key to get it ready for streaming the programme
- Activate Jitsi's streaming to Youtube Live by using the pre-configured stream key
- Click 'Go Live' in Youtube Studio
- Organise a monitoring window with: OBS, terminal with `v4l2loopback` script, Twitter, Youtube Live preview, and a paper programme with public/private talks

For private calls:
- Stop streaming on Jitsi and start OBS streaming of the programme
- When finished, stop OBS streaming and restart streaming on Jitsi
