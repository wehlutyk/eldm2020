#!/bin/bash -ex

module_id_by_sinkname() {
    pacmd list-sinks | grep -e 'name:' -e 'module:' | grep -A1 "name: <$1>" | grep module: | cut -f2 -d: | tr -d ' '
}

GST_AUDIO_MIMETYPE="audio/x-raw-int"
GST_AUDIO_FORMAT="width=16,depth=16,endianness=1234,signed=true"
GST_AUDIO_RATE="rate=44100"
GST_AUDIO_CHANNELS="channels=1"
GST_AUDIO_LAYOUT=""

GST_AUDIO_CAPS="$GST_AUDIO_MIMETYPE,$GST_AUDIO_FORMAT$GST_AUDIO_LAYOUT,$GST_AUDIO_RATE,$GST_AUDIO_CHANNELS"
PA_AUDIO_CAPS="" #"$GST_AUDIO_FORMAT $GST_AUDIO_RATE $GST_AUDIO_CHANNELS"

DEFAULT_SINK=$(pacmd dump | grep set-default-sink | cut -f2 -d " ")
DEFAULT_SOURCE=$(pacmd dump | grep set-default-source | cut -f2 -d " ")

SINK_NAME="streamaudio"
SINK_ID=$(module_id_by_sinkname $SINK_NAME)
ECANCEL_ID=$(module_id_by_sinkname "${SINK_NAME}_echo_cancel")

if [ -z $SINK_ID ] ; then
    SINK_ID=$(pactl load-module module-null-sink \
                    sink_name="$SINK_NAME" \
                    $PA_AUDIO_CAPS \
                    sink_properties="device.description='Stream-audio'")
fi
echo "Sink pulseaudio id: $SINK_ID"

if [ -z $ECANCEL_ID ] ; then
    ECANCEL_ID=$(pactl load-module module-echo-cancel \
                       sink_name="${SINK_NAME}_echo_cancel" \
                       source_master="$SINK_NAME.monitor" \
                       sink_master="$DEFAULT_SINK" \
                       $PA_AUDIO_CAPS \
                       aec_method="webrtc" save_aec=true use_volume_sharing=true) || true
fi
echo "Echo cancellation pulseaudio id: $ECANCEL_ID"

pactl set-default-source $SINK_NAME.monitor

gst-launch-0.10 -v filesrc location=/home/sl/Videos/Webcam/2018-10-22-214447.webm do-timestamp=true ! matroskademux name=f \
  f.audio_00 ! queue ! vorbisdec ! audioconvert ! audioresample ! $GST_AUDIO_CAPS ! pulsesink device="$SINK_NAME" sync=true \
  f.video_00 ! queue ! vp8dec ! autovideoconvert ! v4l2sink device=/dev/video2 sync=true &
# f.audio_00 ! queue ! vorbisdec ! audioconvert ! audioresample ! autoaudiosink

GSTLAUNCH_PID=$!

echo "Press enter to end stream"
perl -e '<STDIN>'

kill $GSTLAUNCH_PID > /dev/null 2>&1 || echo ""
pactl set-default-source ${DEFAULT_SOURCE}
pactl unload-module ${ECANCEL_ID}
pactl unload-module ${SINK_ID}
