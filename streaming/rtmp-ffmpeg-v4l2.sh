#!/bin/bash -e

if [ $# != 1 ]; then
    echo "Usage: $(basename $0) RTMP_URL"
    exit 1
fi

RTMP_URL=$1

module_id_by_sinkname() {
    pacmd list-sinks | grep -e 'name:' -e 'module:' | grep -A1 "name: <$1>" | grep module: | cut -f2 -d: | tr -d ' '
}
module_id_by_sourcename() {
    pacmd list-sources | grep -e 'name:' -e 'module:' | grep -A1 "name: <$1>" | grep module: | cut -f2 -d: | tr -d ' '
}

echo -n "Looking for v4l2loopback video device... "
for device in $(ls /dev/video*); do
    if v4l2-ctl -d $device -D | grep -q v4l2loopback ; then
        VIDEO_DEVICE=$device
    fi
done
if [ x$VIDEO_DEVICE == 'x' ]; then
    echo "not found. Aborting."
    exit 1
else
    echo $VIDEO_DEVICE
fi

DEFAULT_SOURCE=$(pacmd dump | grep set-default-source | cut -f2 -d " ")
echo "Original default source: $DEFAULT_SOURCE"

SOURCE_NAME="virtmic"
SOURCE_ID=$(module_id_by_sourcename $SOURCE_NAME)

if [ -z $SOURCE_ID ] ; then
    SOURCE_ID=$(pactl load-module module-pipe-source \
                      source_name="$SOURCE_NAME" \
                      file=/tmp/virtmic \
                      format=s16le rate=16000 channels=1)
fi
echo "Created pulseaudio source '$SOURCE_NAME' with id $SOURCE_ID"
echo "Setting it as default source"
pactl set-default-source $SOURCE_NAME

echo "Starting ffmpeg streaming from '$RTMP_URL' -- press 'q' to quit"
ffmpeg -i $RTMP_URL \
       -map 0:a -f s16le -ar 16000 -ac 1 /tmp/virtmic \
       -map 0:v -f v4l2 $VIDEO_DEVICE
       # -map 0:a -f pulse -ar 16000 -ac 1 -device "$SINK_NAME" "Stream-audio" \
# Then press 'q' to stop the ffmpeg process

echo "Resetting the default pulseaudio source to '$DEFAULT_SOURCE'"
pactl set-default-source $DEFAULT_SOURCE
echo "Deleting pulseaudio source '$SOURCE_NAME' (id $SOURCE_ID)"
pactl unload-module $SOURCE_ID
echo "All done"
