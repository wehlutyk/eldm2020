title: An Embodied and Enactive Approach to Experiential (Contemporary) Art
authors: Alice Dupas
---

Because the experience of a contemporary work of art prominently involves the viewer’s body, one should propose a new designation for both contemporary art and the viewer. The latter becomes a participant –others would say a “*visiteur*” (visitor) (Raboisson, 2013) or a “*fureteur*” (brower) (Lejeune, 2012)– whose body operates on the work to such an extent that art is characterized by its experiential nature. In contemporary/experiential art, as O'Reilly puts it : “*aucun spectateur n’[est] toléré – tout le monde [doit] participer*” (2009, p. 197) (no spectator is tolerated -everyone has to participate).

Following the artist N. Raboisson, one could say that experiential art is itself of several kinds. If certain works depend, in order to be well apprehended, on the intervention of a spectator and more fundamentally on his active experience, they will not fundamentally change without this action. They consequently fall within the scope of “*spect-aculaire*” (spect-acular) (2013). Nevertheless, there is another form of experiential art that involves a different bodily engagement and that modifies intrinsically the work: interactive art. Raboisson's detailed analysis of interactive art is quite conducive to understanding the constitutive dependence that is played out between the participant's body and the work of art. Raboisson attributes to this interactive art the characteristic of being actualized by the participant as opposed to spectacular art whose meaning is already fixed by its author. And from her perspective, the interactive work of art is actualized *at the very moment* the individual participates in the work. One should say however that it is not because the moments of creation and reception are more widely dissociated in other forms of contemporary art that the actualization is less effective and thus less true. In this sense, even contemporary art that is not specifically interactive *is* still experiential.

The theoretical model that best captures this artistic approach is *enaction*. F. Lejeune (2012) and J. Hall (2014), for example, have chosen enaction to theorize contemporary art. These congruent approaches to experiential art are fundamentally embodied and enactive approaches to art based on the concept of *embodied experience*. They shed light on the active dimension that the artistic environment plays on the mobility of the body in the aesthetic experience. Aesthetic experience thus appears as a kind of embodied and enactive interaction with the work of art, as a situation of structural coupling between the participant and the artistic environment.

References
----------

Colombetti G. & Thomspon E., 2010, “The feeling body. Toward an enactive approach to emotion”, W. F. Overton et al., *Developmental Perspectives on Embodiment and Consciousness*, pp. 45-68.

Hall J., 2014, “Interactive Art and the Action of Behavioral Aesthetics in Embodied Philosophy”, *Academic Research and Dissertations*, Book 4.

Hutto D. D., 2015, “Enactive Aesthetics : philosophical reflections on artful minds”, A. Scarinzi, *Aesthetics and the Embodied Mind: Beyond Art Theory and the Cartesian Mind-Body Dichotomy*, p. 211-227.

Johnson M., 2007, *The Meaning of the Body*, The University of Chicago Press, Chicago.

Lejeune F., 2012, “Corps à corps œuvre-public. Approche esthétique d’une installation”, *Revue Proteus, Cahiers des théories de l’art*, n°1, p. 19-28.

Noë A., 2015, *Strange Tools, Art and Human Nature*, Hill and Wang, New York.

O’Reilly S., 2009, *Le corps dans l’art contemporain*, Thames & Hudson, Paris.

Raboisson N., 2013, “L’expérience fait œuvre : l’action corporelle créatrice”, *Revue Proteus, Cahiers des théories de l’art*, n°6, p. 35-42.

Scarinzi A., 2015, *Aesthetics and the Embodied Mind: Beyond Art Theory and the Cartesian Mind-Body Dichotomy*, Springer, London.

Scarinzi A., 2017, “Skillful Aesthetic Experience and the Enactive Mind”, *Metis, XXIV*, n°1, pp. 163-176

Varela F. et al., 1999, *L’inscription corporelle de l’esprit : sciences cognitives et expérience humaine*, Seuil, Paris.

Authors
-------

* Alice Dupas: ENS/Lyon – IHRIM/Triangle

[//]: # (license: confirmed)

**License:** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
