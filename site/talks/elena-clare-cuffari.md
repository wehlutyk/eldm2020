title: Language and the Future of Bodies
authors: Elena Clare Cuffari
video-status: public
video-peertube: 75bf24f3-794f-4dc9-94a4-4587deebfc46
video-youtube: jWLAZo8rFLo
---

What are bodies? Elena Clare Cuffari, Assistant Professor of Philosophy at Worcester State University, outlines a theory of bodies and an intersectional and dialectical approach to managing tensions that emerge from the entanglement of diverse bodies. This new account explains humans, language, and mind in terms of continuity with all life. Humans are literally billions of different bodies. Living in language is a constant, open-ended negotiation of past, present, and future; of other bodies’ perspectives; of habit and freedom.

In this talk, Cuffari will ask whether a more accurate and humane ontology of bodies could aid humans in living benevolently with the world at large. She will discuss the recent enactive proposal of linguistic bodies that arises from a methodological commitment, unique in the cognitive sciences, to stay open to bodies in their inherent multiplicity, concreteness, difference, and unfinishedness. By reflecting on linguistic habits—particularly regarding the environment and technology—Cuffari will consider how communities may realize (or short-circuit) the dynamic potentiality in which we collectively abide each day.

[//]: # (license: confirmed)

**License:** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
