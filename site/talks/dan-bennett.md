authors: Daniel Bennett, Anne Roudaut & Oussama Metatla
title: Multifractal Measures of Embodied Interaction with Technology
---

In this presentation we discuss multifractal analysis as a promising tool for developing quantified measures of embodied interaction with technology. We introduce research from embodied and enactive cognitive science which shows that multifractal patterns are ubiquitous in human behaviour, and that they vary according to the individual's adaptation to the task context. Against this backdrop we discuss our own work on multifractal patterns in technology use, pointing to the ways in which these patterns may be informative about a range of higher-level behavioural features, including skill learning and level-of-engagement.

Multifractal patterns have been shown to occur in systems where behaviour is not centrally driven but instead arises from coordination between distributed components. Greater degrees of coupling between distributed components results in stronger multifractal signatures. The observation of these patterns in a range of human behaviours has been taken as evidence for a dynamical, rather than computational, account of cognitive behaviour. More recently, research by ourselves and others has shown that changes in these patterns correspond to a range of behavioural phenomena - task engagement, skill learning, and even the Heideggerian notion of the "readiness to hand" of tool use.

Multifractal patterns have been found in a huge range of human behaviours where there is need to adapt to a complex behavioural context - from language activities such as verbal conversation, and essay typing, to more obviously physical activities including coordinated team sports like rowing. As such we suggest that multifractal metrics have great potential as an embodied metric of interaction with technology, where they can augment existing, generally qualitative, approaches to understanding embodied interaction with technology. As such mutlifractal approaches could support the development of dynamic, adaptive technologies grounded in theories of embodiment.

Authors
-------

* Daniel Bennett: Bristol Interaction Group - University of Bristol, UK
* Anne Roudaut: Bristol Interaction Group - University of Bristol, UK
* Oussama Metatla: Bristol Interaction Group - University of Bristol, UK

[//]: # (license: confirmed)

**License:** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
