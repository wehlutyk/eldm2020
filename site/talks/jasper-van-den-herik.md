title: Situated Normativity in Languaging
authors: Jasper van den Herik & Erik Rietveld
video-status: public
video-peertube: 60e24024-48f2-4d7c-bba4-f838fa7627df
video-youtube: wri-DVDSpCs
---

Languaging is a normative phenomenon: it is subject to norms, or standards of correctness and appropriateness. Think for example of calling this colour → ■ black, or the rules of grammar. In this paper, we characterise normativity in languaging in a way that foregrounds both its embodied and socio-material nature.

We do so by understanding the normativity of language as a form of situated normativity, which is the ability of skilled individuals to distinguish better from worse in the context of a particular situation (Rietveld 2008). Situated normativity is expressed in situated appreciations. A situated appreciation can be experienced as a bodily affective tension that motivates a skilled individual to act on particular possibilities for action, called affordances, offered by a concrete socio-material situation (Rietveld & Kiverstein 2014). Which affordances motivate an individual is determined by their embodied skills and habits that have been shaped in a history of learning in socio-material practices.

In a first step, building on an example given by Wittgenstein (1967) of architects at work, we show how languaging allows us to jointly articulate a situated normative appreciation of the concrete situation. This example foregrounds that our appreciations often emerge from interaction with others and the socio-materiality of the concrete situation. In a second step, we build on the idea that reflexivity is one of the essential elements of languaging (e.g. Dingemanse et al. 2015; Taylor 2000). We characterise reflexivity as situated and skilfully performed activity afforded by sociomaterial languaging practices (cf. Noë 2015). Combining step one and two, we argue that the normative dimension of languaging behaviour is subject to ongoing determination and (re)negotiation (cf. Di Paolo, Cuffari, & De Jaegher 2018): norms in languaging are not pre-given and context-independent rules that govern human behaviour, but emerge from situated appreciations in interaction and are enacted and enforced in concrete situations.

References
----------

Di Paolo, E., Cuffari, E.C., & De Jaegher, H. (2018). *Linguistic Bodies: The Continuity Between Life and Language*. Cambridge, Ma.: The Mit Press.

Dingemanse, M., Roberts, S.G., Baranova, J., Blythe, J., Drew, P., Floyd, S., Gisladottir, R.S., Kendrick, K.H., Levinson, S.C., Manrique, E., Rossi, R., & Enfield, N.J. (2015). Universal Principles in the Repair of Communication Problems, *PLoS ONE*, 10(9), 1–15. doi.org/10.1371/journal.pone.0136100

Noë, A. (2015). *Strange Tools: Art and Human Nature*. Hill & Wang.

Rietveld, E. (2008). Situated Normativity: The Normative Aspect of Embodied Cognition in Unreflective Action. *Mind*, 117(468), 973–1004. doi.org/10.1093/mind/fzn050

Rietveld, E. & Kiverstein, J. (2014). A rich landscape of affordances. *Ecological Psychology*, 26(4), 325–352. doi.org/10.1080/10407413.2014.958035

Taylor, T.J. (2000). Language constructing language: the implications of reflexivity for linguistic theory. *Language Sciences*, 22, 483–499. doi.org/10.1016/S0388-0001(00)00016-4

Wittgenstein, L. (1967). *Lectures and Conversations on Aesthetics, Psychology and Religious Belief*. Blackwell.

Authors
-------

* Jasper van den Herik: AMC / University of Amsterdam
* Erik Rietveld: AMC / University of Amsterdam

[//]: # (license: confirmed)

**License:** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
