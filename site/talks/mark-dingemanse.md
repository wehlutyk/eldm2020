title: Brain-to-Brain Interfaces and the Role of Language in Distributing Agency
authors: Mark Dingemanse
video-status: public
video-peertube: 057fa06e-eade-4f08-b3b4-420519294488
video-youtube: uojoX_SChKM
---

Much excitement surrounds prototypes of brain-to-brain interfaces, which aim to directly connect brains and enable new forms of communication and collaboration. An implicit goal in this line of work is to escape the pesky limits of human language, with its potential for deception, imprecision and misunderstanding. I juxtapose this pessimistic view of language with a more constructive perspective. Human language is in some ways the ultimate brain-to-brain interface: a system for interactive coupling that enables individuals with separate bodies to achieve joint agency, without giving up behavioural flexibility and social accountability.

Two features of language stand out as crucial in human social life: (i) its selectivity, giving people control over the relation between private worlds and public words; and (ii) its negotiability, giving people systematic opportunities for calibrating and revising joint commitments. Language serves as a filter between the private and the public, and as an infrastructure for negotiating consent and dissent. As research into brain-to-brain interfaces matures, it is my hope that it will find ways to incorporate selectivity and negotiability, so as to extend human agency in meaningful, ethical, and humane ways.​

[//]: # (license: default option)

**License:** all rights reserved
