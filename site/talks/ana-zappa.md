title: Does embodied L2 verb learning affect retention and representation?
authors: Ana Zappa, Deidre Bolger, Jean-Marie Pergandi, Raphael Fargier, Daniel Mestre & Cheryl Frenck-Mestre
---

Embodied semantics binds physical context and action to cognition, and hence physical movement performed by learners could play an important role in second language (L2) learning outcome. The objective of the present study is to observe how naturalistic actions in a highly immersive, multimodal, interactive 3D virtual reality (VR) environment may enhance fast mapping. We are interested in whether “embodied learning”, or learning that occurs using specific physical movements that are coherent with the meaning of new words, creates linguistic representations that produce greater motor resonance due to stronger motor traces compared to simply pointing, and whether they are better retained.

Learning will take place in a VR environment using an Oculus headset and controller. Half of the participants will learn the auditory action verbs by performing specific movements to manipulate objects following the observation of the movements on a CRT screen. The other half will learn the new words by observing the same action animations and then pointing to the object on which they were performed. The neural correlates of embodied learning will be investigated using EEG to in two separate tasks, both pre and post training. In the first task, we will measure motor activation using time-frequency analyses while participants listen to newly learned words. In the second task, the semantic processing of newly learned words will be measured using a match/mismatch experiment. The behavioral correlates of this type of learning (word-meaning retention) will also be recorded.

We expect words learned in the Specific action condition to produce greater motor resonance post-training than those learned in the Pointing condition, due to a stronger sensorimotor trace. We also expect learning to be enhanced by specific movements as shown by a greater N400 effect for Mismatch versus Match pairs for words learned in the specific action condition, as well as better behavioral learning outcomes. This would provide evidence in favor of theories of embodied semantics.

[//]: # (license: default option)

**License:** all rights reserved
