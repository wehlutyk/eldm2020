authors: Audrey Mazur-Palandre, Jean-Marc Colletta & Kristine Lund
title: “Here we go, I’m going to explain how we play” Multimodal, procedural explanation from a developmental perspective
video-status: editing
---

During the production of an oral explanation, a speaker is confronted with a task involving two main objectives. First, the referential inherent in the object of the explanation must be constructed. Second, the interlocutors and the interaction itself must be managed. Given this, our objective was to first study how a child manages a procedural explanation with an interactional aspect. But we were also interested in understanding the developmental trajectory of these referential and pragmatic constraints of language production. When do they begin to be mastered? We thus collected and analyzed multimodal explanations from children aged 9 to 13. The results show that age has an impact on the information and gestural choices of children, that children rely on the gestural modality of language differently according to age and finally that as they grow, they improve the management of interactional constraints.

Authors
-------

* Audrey Mazur-Palandre, Laboratoire ICAR (UMR5191, CNRS, Université Lyon 2 et ENS de Lyon), Laboratoire d’Excellence ASLAN
* Jean-Marc Colletta, Laboratoire LIDILEM (EA 609, CNRS et Université Grenoble Alpes)
* Kristine Lund, Laboratoire ICAR (UMR5191, CNRS, Université Lyon 2 et ENS de Lyon), Laboratoire d’Excellence ASLAN

[//]: # (license: default option)

**License:** all rights reserved
