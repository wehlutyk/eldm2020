authors: Alexandre Aguila, Abdallah Bacar, Julien Deseigne, Gérald Gurtner, Thibault Lara & Camille Marie
title: Cultural rights with 3D vocal prints
---

*Note: this proposal combines a poster with an exhibition to experiment the kind of interactions that can be designed with vocal prints.*

The way people communicate information is not 100% efficient because of cultural and social differences or even simply misunderstanding. We have experimented a way of materializing such complexity in human relations in a metaphorical way to promote cultural rights with homeless people. Such materialization relies on two complementary processes :

* An inclusive and creative process in three steps to fabricate original machines or art installations. These steps are ideation within a group, collective decision and cooperative fabrication.
* A technological process focused on the transformation of people's voices in 3D physical objects.

The fragile balance between both processes results in cross-fertilizing knowledge, experiences, skills, testimonies between homeless people, researchers, social workers, artists and so on. Consequently each people learn from each other and a sense of community by making objects arises which allows to overcome social isolation, a by-product of the gradual impoverishment of the interactions between these people and the rest of the society, due to the focus on immediate needs.

This echoes to what people living in difficult conditions share when they talk about cultural rights : « Culture is to communicate by the language of art, traditions, values, experiences to be able to resist to an accelerating society who excludes lot of people ». Listening to people and creating an object to materialise it brings both metaphorically and literally their voice to the outside world.

We will present the cultural project « I see what you say » lead by the organisation Démostalie and its partners to illustrate the materialization of voice for promoting cultural rights through cross-fertilization of knowledge. We will discuss the organisational dynamicst to support a continuous link with homeless people and the relevance of considering such projects as boundary objects (concept developped in anthropoly of knowledge by Susan Leigh Star and Jim Griesemer in the late 1980s).

Authors
-------

Authors are co-presidents of the social organization Démostalie. Supported by Coexiscience, Foyer Notre-Dame des Sans-Abri and Green Bees.

[//]: # (license: default option)

**License:** all rights reserved
