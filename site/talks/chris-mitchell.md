title: Constructing Speech, a THEMPPO Silent Text Approach
authors: Chris Mitchell & Marieke de Koning
video-status: private
---

With the challenges it poses in teaching and evaluation prosody has been much neglected in language teaching (Lengeris 2012). Given its importance in conveying structural information that can change meaning this is a lacuna which needs to be addressed. A better understanding of the roles played by the body and the voice in oral communication has been identified as an essential factor in enabling teachers to incorporate work on prosody in the classroom (Mitchell et al. 2018).

To this end the action research work of THEMPPO* places the body of the learner at the source of what is happening in oral communication, preparing the learner to recognize the importance of the corporal and vocal elements involved in the dynamics of speech. The incorporation of collaborative dialogue, which has already been identified as a source of second language learning and development (Swain and Wantanabe 2013), is part of this work, revealing the close link between cognition and emotion, the latter playing an important role in prosody.

THEMPPO Silent Texts incorporate the two THEMPPO praxes, essential for awareness, used in teacher training workshops: the Silent Experience which explores the role of silence before and during speech, and the Engaged Body which focuses on the interaction between speakers, listeners, and the environment in which they interact. THEMPPO Silent Texts are theatre texts with no dialogue but with stage directions. Coactors follow these stage directions, in which speed of movement, occupation of the space and the nature of interaction between the coactors are all features which are clearly prescribed. Moments when speech then takes place are also detailed. The coactors are taking part in a shared cognitive recognition of the nature of their interaction and in a process of co-construction of emotions. Pepin (2008) describes emotions as being, in the process of social interactions, jointly constructed,deconstructed and reconstructed by the participants. Finally, out of the work of the coactors comes a collaborative dialogue incorporating speech and movement.

These texts, with their origins in theatre improvisation, demonstrate the roles of cognition and emotion in collaborative dialogue.

\* The THEMPPO (Thématique Prosodie et Production Orale) group at the UGA Grenoble University, France, has been engaged, since 2013, in an action research programme aimed at responding to the poor results obtained in oral production in language learning at university level. Prix PEPS 2018 for Pedagogical Research

References
----------

Lengeris, Angelos. (2012). Prosody and Second Language Teaching: Lessons from L2 Speech Perception and Production Research. 10.1007/978-94-007-3883-6_3.

Mitchell, C., M. de Koning & R. Guy (2018) The “Silent Experience”: A New Approach to Prosody, Recherche et Pratiques Pédagogiques En Langues de Spécialité - Cahiers de l’Apliut, 37, 2. DOI: 10.4000/apliut.6367

Pepin, N. (2008) Studies on emotions in social interactions, *Bulletin Suisse de Linguistique Appliquée* (VALS-ASLA), 88, p. 1-18.

Swain, Merrill & Watanabe, Yuko. (2013). Languaging: Collaborative dialogue as a source of second language learning. 10.1002/9781405198431.wbeal0664.

Authors
-------

* Chris Mitchell, voice coach, theatre and English teacher. Member of the THEMPPO team since 2013.
* Marieke de Koning, dancer, specialist of the body in movement. English teacher. Member of THEMPPO since 2016.

[//]: # (license: default option)

**License:** all rights reserved
