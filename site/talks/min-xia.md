authors: Min Xia
title: Embodied Cognition Still Needs Mental Representation
---

> “Mental representation is a theoretical assumption, not a commonplace of ordinary discourse. …To understand the notion of mental representation, one must understand the explanatory role that framework assigns to mental representation.”
– Robert Cummins, Meaning and Mental Representation

Brooks’ (1991) “intelligence without representation” epitomized the core theoretical shift of cognitive science from computationalism to embodiment. But embodiment is not a clear-cut concept – it varies from minimal to radical versions in terms of the weight it gives to the explanatory utility of representation (Gallagher, 2011). On the other hand, representation also embodies a cluster of different conceptions. Hence, in order not to get into a position of for or anti-representation “for the sake of appearance” (Haselager et al., 2003), it is incumbent to sort out variant meanings of representation and set clear objectives for debate.

I consider representation as being significant in understanding cognition and argue that embodiment and representation can reach compatibility. To defend my views, I will first examine the popular criticisms mounted against representation, from perspectives concerning its form, its content and its function. A careful survey shows that what anti-representationalists debunked is the conception of representation manufactured in the cognitivist framework. Second, picking up the thread of Haugeland's (1991) representational genera, I distinguish two categories of representation, (1) the traditional referential representations proposed by classical cognitivism and (2) the enactive representation that is realized in a dynamic system, for example, Bickhard’s (1998) interactive representation, Rosenberg & Anderson's (2004) guidance theory of representation. I attempt to give a full-scale, systematic and ecological account of representation that spans different levels of cognition and show that enactive representation fits well in the embodied approach. Third, I use the pioneering predictive processing model as a case study to show that enactive representation can be perfectly integrated in an embodied dynamic system without being subject to the criticisms from the radicals. Finally, I conclude that representation has its explanatory utility in embodied cognitive science.

My motivation can be put as follows: siding with non-radical embodiment, I am dedicated to reforming the debate of representation and attempting to integrate enactive representations in the embodied framework.

Authors
-------

* Min Xia: ENS de Lyon/East China Normal University

[//]: # (license: confirmed)

**License:** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
