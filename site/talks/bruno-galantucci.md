authors: Bruno Galantucci
title: The (un)faithfulness of human informational exchanges
video-status: editing
---

Common-sense intuition suggests that, when people are engaged in informational exchanges, they communicate so as to be reasonably sure that they perform the exchanges faithfully. Over the years, my colleagues and I have found evidence suggesting that this intuition, which is woven into several influential theories of human communication, may be misleading. I first summarize the evidence, which comes partly from experimental semiotics studies and partly from studies of ordinary conversations. I then discuss the potential limitations of this evidence and present a set of two new studies that addresses them.

In both studies, a confederate instructed participants to “pick up the skask” from a tray containing six objects and move it to a specific location. Since skask is a non-word invented by us, participants had to ask for clarification to perform the instruction faithfully. In contradiction with the intuition that people pursue faithfulness when engaged in informational exchanges, 29 of the 48 participants we tested across the two studies performed the instruction without asking for clarification. This behavior, which occurred even when avoiding the clarification was likely to entail overt consequences, occurred more frequently when avoiding the clarification was less consequential. Other factors such as individual differences and people’s perceptions of the social context, if they played a role at all, they did it to an extent that was unlikely to be comparable to that of the role played by overt consequences.

Considered together, our various assessments of the extent to which people engage in faithful informational exchanges converge on a simple conclusion: Communicating faithfully is a substantially demanding task, and people often fail at it. I discuss the implications of this conclusion and speculate on its relevance for understanding the evolutionary past of human communication.

Authors
-------

* Bruno Galantucci: Yeshiva University & John Cabot University

[//]: # (license: confirmed)

**License:** [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
