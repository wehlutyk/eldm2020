title: Haptic languaging: turn-taking and meta-coordination
authors: Vincenzo Raimondi & Charles Lenay
video-status: private
---

The notion of languaging emphasizes the embodied, dynamic nature of linguistic activity and its constitutive role for social interaction. Coordination through languaging is an interactional achievement that draws on multiple bodily resources and involves cognitive, affective, and experiential dimensions. Moreover, languaging allows for coordination about coordination (recursive coordination) while doing things together.

In this paper, we explore linguistic coordination within the framework of technologically- mediated communication. By participating in the active constitution of specific forms of perception and action, technological mediation allows the emergence of new domains of interaction. Our original, minimalist experimental system is based on haptic devices (MIT4-Tactos) developed by Charles Lenay and his team at Costech (Université de Technologie de Compiègne). Our hypothesis is that fundamental features of linguistic coordination such as turntaking and meta-coordination would emerge spontaneously as soon as participants engage in technologically-mediated tactile interactions— even though these are unlike any form of languaging they have ever experienced before.

In our research, participants were given the task to create tactile signs and negotiate their meaning, by interacting through the haptic devices. We have conducted research under different conditions. A first group was given signs for meta-coordination, in order to help them to carry the task of negotiation. A second group was asked to carry the same task but no signs for meta-coordination were given. We have observed that in the second group, participants spontaneously created dedicated signs to recursively regulate the ongoing coordination. It seems, therefore, that languaging processes follow general principles that give rise to the emergence of meta-coordination.

Authors
-------

* Vincenzo Raimondi: Université de Technologie de Compiègne - Laboratoire Costech
* Charles Lenay: Université de Technologie de Compiègne - Laboratoire Costech

[//]: # (license: default option)

**License:** all rights reserved
