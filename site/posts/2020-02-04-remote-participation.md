title: Remote participation
date: 2020-02-04 18:00
tags: remote, participation
summary: If you can't make it physically to Lyon, check out how you can participate remotely!
---

Thanks to a few requests, the need for remote options for participating in the workshop has made itself clear. So we're very pleased to announce that this will be made possible through a combination of conference call and live streaming. Check out the [Remote](https://wehlutyk.gitlab.io/eldm2020/remote.html) page for more details!
