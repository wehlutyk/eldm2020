title: Deadlines updated
date: 2019-11-27 18:00
tags: deadlines
summary: The workshop deadlines and publication dates have been udpdated (in a good way)
---

We just relaxed the deadlines for submitting contributions and registering to the workshop.

The initial dates seemed a little tight, so here are the new ones:

- Submission deadline: **6th January 2020** (changed from 31st December 2019)
- Notification of acceptance: **13th January 2020** (changed from 10th January)
- Registration opens: **1st January 2020** (changed from 11th January 2020)

If you had noted the previous dates in your calendar, make sure to update. But if this is your first time on the workshop's website, you can safely ignore this post.

Happy preparations!
