title: Recorded talks are available
date: 2020-03-25 18:00
tags: recordings
summary: The first recorded talks are available
---

The first recorded talks from the workshop are now available! Visit the [full programme](https://wehlutyk.gitlab.io/eldm2020/programme.html#programme) to see which talks can be viewed. A few talks are still being edited and will be available shortly.
