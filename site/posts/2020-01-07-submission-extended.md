title: Submission deadline extended
date: 2020-01-07 18:00
tags: registration
summary: We extended the submission deadline!
---

Since we haven't yet reached capacity, we are extending the deadline for submitting proposals to the workshop. Please read the [Call](https://wehlutyk.gitlab.io/eldm2020/call.html) for more details, and don't hesitate to [ask any questions](mailto:sebastien.lerique@normalesup.org) if in doubt!
