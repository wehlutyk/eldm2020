title: Registration open
date: 2020-01-01 18:00
tags: registration
summary: Registration has opened!
---

Registration just opened. Please make sure to fill the [Registration form](https://eldm2020.sciencesconf.org/registration) to attend the workshop! No need to have submitted a proposal to the Call, anybody can participate.
