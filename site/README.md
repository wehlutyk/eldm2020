ELDM2020 Website
================

Built using [Haunt](http://haunt.dthompson.us/), which you must install before starting.

Build with `haunt build`, build and serve continuously with `haunt serve -w`.

Since this website sits under a base url (it's at wehlutyk.gitlab.io/eldm2020), when building for production you can use `export BASEURL=/eldm2020 haunt build`.
