(use-modules (haunt asset)
             (haunt site)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
	     (haunt page)
	     (haunt html)
	     (haunt post)
	     (haunt reader)
             (haunt reader commonmark)
	     (commonmark)
	     (srfi srfi-11)
	     (srfi srfi-19)
	     (srfi srfi-9)
	     (ice-9 ftw)
             (ice-9 match)
	     (ice-9 textual-ports))

(define %submission-deadline-date     "20th January 2020 (Anywhere on Earth)")
(define %acceptance-date              "22nd January 2020")
(define %registration-open-date       "1st January 2020")
(define %registration-close-date      "10th February 2020")
(define %contributions-published-date "24th January 2020")
(define %programme-published-date     "31st January 2020")
(define %registration-remote-date     "17th February 2020")
(define %workshop-date                "18th February 2020")

(define* (link name uri #:key attrs)
  `(a ,(cons '@ (cons `(href ,uri) (or attrs '()))) ,name))

(define %email-sl "sebastien.lerique@normalesup.org")
(define (mailto-sl name)
  (link name (string-append "mailto:" %email-sl)))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      "CC-BY-SA-4.0"))

(define %cc-by-sa-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-sa/4.0/80x15.png")))))

(define %new-content-dot
  '(span (@ (class "new-content")) ""))

(define %base-url
  (or (getenv "BASEURL") ""))

(define %logo-ens
  `(a (@ (href "//www.ens-lyon.fr/") (class "institution-logo"))
      (img (@ (src ,(string-append %base-url "/images/ens.svg"))))))

(define %logo-ixxi
  `(a (@ (href "//www.ixxi.fr/") (class "institution-logo"))
      (img (@ (src ,(string-append %base-url "/images/ixxi.png"))))))

(define %logo-lip
  `(a (@ (href "//www.ens-lyon.fr/LIP/") (class "institution-logo"))
      (img (@ (src ,(string-append %base-url "/images/lip.png"))))))

(define %matomo-code
  `((script (@ (type "text/javascript") (src ,(string-append %base-url "/js/matomo.js"))))
    (noscript
     (p (img (@ (src "https://matomo.slvh.fr/matomo.php?idsite=1&rec=1")
		(style "border:0;")
		(alt "")))))))

(define %playlist-peertube-link
  (link "PeerTube" "https://peertube.co.uk/videos/watch/playlist/b5cdbeea-43b0-4462-ab73-24ff81d7a22d"))

(define %playlist-youtube-link
  (link "YouTube" "https://www.youtube.com/playlist?list=PLEhNQHmIwzjCKXYUPWqyc6zTRid8a6aV1"))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append %base-url "/css/" name ".css")))))

(define* (link/inner name uri #:key attrs)
  (link name (string-append %base-url uri) #:attrs attrs))

(define (string-replace-substring s substring replacement)
  "Replace every instance of @var{substring} in string @var{s} by @var{replacement}."
  (let ((sublen (string-length substring)))
    (with-output-to-string
      (lambda ()
        (let lp ((start 0))
          (cond
           ((string-contains s substring start)
            => (lambda (end)
                 (display (substring/shared s start end))
                 (display replacement)
                 (lp (+ end sublen))))
           (else
            (display (substring/shared s start)))))))))

(define (string->anchor title)
  (string-downcase
   (string-replace-substring title " " "-")))

(define (title-link tag title)
  `(,tag (@ (id ,(string->anchor title)))
    (a
     (@
      (class "anchor")
      (href ,(string-append "#" (string->anchor title)))
      (aria-hidden "true"))
     (i (@ (class "fas fa-link"))))
    ,title))

(define-record-type <speaker>
  (make-speaker name website institution blurb talk)
  speaker?
  (name        speaker-name)
  (website     speaker-website)
  (institution speaker-institution)
  (blurb       speaker-blurb)
  (talk        speaker-talk))

(define (speaker-image-url speaker)
  (string-append
   %base-url
   "/images/"
   (string->anchor (speaker-name speaker))
   "-original.jpg"))

(define-record-type <talk>
  (make-talk title authors video-status video-peertube video-youtube abstract)
  talk?
  (title          talk-title)
  (authors        talk-authors)
  (video-status   talk-video-status)
  (video-peertube talk-video-peertube)
  (video-youtube  talk-video-youtube)
  (abstract       talk-abstract))

(define (speaker-title speaker)
  (talk-title (speaker-talk speaker)))

(define (speaker-abstract speaker)
  (talk-abstract (speaker-talk speaker)))

(define (talk-slug talk)
  (string-append "/programme/" (string->anchor (talk-authors talk)) ".html"))

(define (read-talk/md path)
  (call-with-input-file (string-append "talks/" path)
    (lambda (port)
      (let* ((metadata (read-metadata-headers port))
	     (sxml (commonmark->sxml port))
	     (video-status (assq-ref metadata 'video-status)))
	(make-talk
	 (assq-ref metadata 'title)
	 (assq-ref metadata 'authors)
	 (if video-status
	     (cond ((string=? video-status "public") 'public)
		   ((string=? video-status "editing") 'editing)
		   ((string=? video-status "private") 'private)
		   (else (error (string-append "unknown video-status for " (assq-ref metadata 'authors)))))
	     #f)
	 (assq-ref metadata 'video-peertube)
	 (assq-ref metadata 'video-youtube)
	 sxml)))))

(define elena
  (make-speaker
   "Elena Clare Cuffari"
   "https://elenaclarecuffari.wordpress.com"
   "Worcester State University"
   "Drawing on a range of disciplines and disciplinary crossings,
but especially working in the enactive approach to mind and language,
Dr. Elena Clare Cuffari investigates the constitutive roles that our
bodies and interactions play in meaning creation and meaning
experience. She is Assistant Professor of Philosophy and Department
Chair of Philosophy at Worcester State University."
   (read-talk/md "elena-clare-cuffari.md")))

(define mark
  (make-speaker
   "Mark Dingemanse"
   "https://www.mpi.nl/people/dingemanse-mark"
   "Radboud University"
   "Mark Dingemanse studies language as shaped by and for socia
 interaction. He is interested in how language enables us to express
ourselves and to act as one with others."
   (read-talk/md "mark-dingemanse.md")))

(define omar
  (make-speaker
   "Omar Rizwan"
   "https://rsnous.com/"
   "Dynamicland"
   `("Omar Rizwan is interested in 'systems' and interfaces. He
currently works as a researcher at "
,(link "Dynamicland" "https://dynamicland.org/") ", a research lab
founded by Bret Victor, building a different kind of computer
system. He lives and works in Oakland, California.")
   (read-talk/md "omar-rizwan.md")))

(define jelle
  (make-speaker
   "Jelle van Dijk"
   "http://www.jellevandijk.org/"
   "University of Twente"
   "Jelle van Dijk is a design-researcher, teacher, writer,
speaker and he draws comics. In his research he investigates the
embodiment of human being in relation to the design of interactive
technology. His current work centers on participatory design for and
with people on the autistic spectrum."
   (read-talk/md "jelle-van-dijk.md")))

(define talks-invited
  (list (speaker-talk elena)
	(speaker-talk mark)
	(speaker-talk omar)
	(speaker-talk jelle)))

(define talk-bruno-galantucci
  (read-talk/md "bruno-galantucci.md"))

(define talk-chris-mitchell
  (read-talk/md "chris-mitchell.md"))

(define talk-didier-bottineau
  (read-talk/md "didier-bottineau.md"))

(define talk-jasper-van-den-herik
  (read-talk/md "jasper-van-den-herik.md"))

(define talk-kris-lund
  (read-talk/md "kris-lund.md"))

(define talk-vincenzo-raimondi
  (read-talk/md "vincenzo-raimondi.md"))

(define talks-contributed
  (list	talk-bruno-galantucci
	talk-chris-mitchell
	talk-didier-bottineau
	talk-jasper-van-den-herik
	talk-kris-lund
	talk-vincenzo-raimondi))

(define poster-alice-dupas
  (read-talk/md "alice-dupas.md"))

(define poster-ana-zappa
  (read-talk/md "ana-zappa.md"))

(define poster-demostalie
  (read-talk/md "demostalie.md"))

(define poster-dan-bennett
  (read-talk/md "dan-bennett.md"))

(define poster-min-xia
  (read-talk/md "min-xia.md"))

(define posters-contributed
  (list poster-alice-dupas
	poster-ana-zappa
	poster-demostalie
	poster-dan-bennett
	poster-min-xia))

(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define* (with-layout theme site title body #:key copyright)
  ((theme-layout theme) site title body #:copyright copyright))

(define eldm-theme
  (theme #:name "eldm"
         #:layout
         (lambda* (site title body #:key copyright)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
	      (meta (@ (name "viewport") (content "width=device-width, initial-scale=1")))
              (title ,(if title
			  (string-append title " — " (site-title site))
			  (site-title site)))
              ,(stylesheet "reset")
              ,(stylesheet "fonts")
              ,(stylesheet "eldm2020")
	      ,(stylesheet "fontawesome")
	      ,(stylesheet "fontawesome-solid"))
             (body
              (div (@ (class "container"))
                   (div (@ (class "nav"))
			(div
			 (ul (li ,(link/inner "ELDM 2020" "/")))
			 (ul
			  (li
			   ,(link/inner "News" "/news")
			   ,(link/inner `(i (@ (class "fas fa-rss-square")))
					"/feed.xml"
					#:attrs '((class "icon-stuck-right"))))
			  (li ,(link/inner "Call" "/call.html"))
			  (li
			   ,(link/inner "Programme" "/programme.html")
			   ,(link/inner `(i (@ (class "fas fa-video")))
					"/programme.html#programme"
					#:attrs '((class "icon-stuck-right"))))
			  (li ,(link/inner "Practicalities" "/practicalities.html"))
			  (li ,(link/inner "Remote" "/remote.html"))))
			(div (@ (class "institution-logos"))
			     ,%logo-ixxi ,%logo-lip ,%logo-ens))
                   (div (@ (class "body")) ,body)
                   (footer
		    (p (@ (class "contact"))
		       "This workshop is organised by "
,(link "Sébastien Lerique" "https://slvh.fr/research/") " and "
,(link "Éric
Guichard" "https://www.enssib.fr/personne/guichard-eric") ", and
supported by " ,(link "IXXI" "//www.ixxi.fr/") " and " ,(link "LIP" "//www.ens-lyon.fr/LIP/") ". Any questions,
please " ,(mailto-sl "ask")
		       ".")
		    (div (@ (class "smallprint"))
			 (p "This website's source code can be found "
			    ,(link "here" "https://gitlab.com/wehlutyk/eldm2020/tree/master/site")
			    ". It is built with "
			    ,(link "Haunt" "http://haunt.dthompson.us")
			    ", whose main page design was copied here,
along with several presentational ideas from "
,(link "IELC2019" "http://www.lel.ed.ac.uk/cle/index.php/ielc2019/") ". Thank
you David Thompson and the Centre for Language Evolution. "
			    (br)
			    ,(or copyright
				 `("Unless otherwise noted, the text is © 2019-2020 Sébastien Lerique, licensed under "
				   ,%cc-by-sa-link "." ,%cc-by-sa-button))))))
              ,%matomo-code)))
         #:post-template
         (lambda (post)
           `((h1 (@ (class "title")),(post-ref post 'title))
             (div (@ (class "date"))
                  ,(date->string (post-date post)
                                 "~B ~d, ~Y"))
             (div (@ (class "post"))
                  ,(post-sxml post))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append %base-url (or prefix "") "/"
                            (site-post-slug site post) ".html"))

           `((h1 ,title)
	     (p "Keep an eye on these posts to be informed of any updates or changes in the organisation. There's an "
		,(link/inner '("RSS feed" (i (@ (class "fas fa-rss-square icon-stuck-right")))) "/feed.xml")
		" to make sure you don't miss anything.")
             ,(map (lambda (post)
                     (let ((uri (post-uri post)))
                       `(div (@ (class "summary"))
                             (h2 (a (@ (href ,uri))
                                    ,(post-ref post 'title)))
                             (div (@ (class "date"))
                                  ,(date->string (post-date post)
                                                 "~B ~d, ~Y"))
                             (div (@ (class "post"))
                                  ,(first-paragraph post))
                             (a (@ (href ,uri)) "read more ➔"))))
                   posts)))))

(define %collections
  `(("News" "index.html" ,posts/reverse-chronological)))

(define* (static-page/sxml title file-name body #:key copyright)
  (lambda (site posts)
    (make-page file-name
               (with-layout eldm-theme site title body #:copyright copyright)
               sxml->html)))

(define (static-page/raw file-name body)
  (lambda (site posts)
    (make-page file-name
	       body
	       display)))

(define fonts-css
  (static-page/raw
   "/css/fonts.css"
   (format
    #f
    (call-with-input-file "css-templates/fonts.css" get-string-all)
    %base-url)))

(define fontawesome-solid-css
  (static-page/raw
   "/css/fontawesome-solid.css"
   (format
    #f
    (call-with-input-file "css-templates/fontawesome-solid.css" get-string-all)
    %base-url)))

(define (speaker/invited speaker)
  `(div (@ (class "speaker-invited"))
	(div (@ (class "image")) (img (@ (src ,(speaker-image-url speaker)) (alt ,(speaker-name speaker)))))
	(div (@ (class "name")) ,(link (speaker-name speaker) (speaker-website speaker)))
	(div (@ (class "institution")) (small ,(speaker-institution speaker)))
	(div (@ (class "title")) (em ,(link/inner (speaker-title speaker) (talk-slug (speaker-talk speaker)))))
	(div (@ (class "blurb")) ,(speaker-blurb speaker))))

(define (speaker/invited-noblurb speaker)
  `(div (@ (class "speaker-invited noblurb"))
	(div (@ (class "image")) (img (@ (src ,(speaker-image-url speaker)) (alt ,(speaker-name speaker)))))
	(div (@ (class "name")) ,(link (speaker-name speaker) (speaker-website speaker)))
	(div (@ (class "institution")) (small ,(speaker-institution speaker)))
	(div (@ (class "title")) (em ,(link/inner (speaker-title speaker) (talk-slug (speaker-talk speaker)))))))

(define index-page
  (static-page/sxml
   #f
   "index.html"
   `((div (@ (class "banner"))
	  (h1 "Embodied interactions, Languaging"
	      (br)
	      "and the Dynamic Medium")
	  (h2 "Lyon, France — " ,%workshop-date))
     (p "A workshop gathering interests and works in embodiment,
languaging, diversity computing and ethical technologies. Recent
developments in these communities are ripe for focused conversations,
cross-pollination and explorations of possible common futures.")
     ,(title-link 'h2 "Talk recordings")
     (p "Visit the " ,(link/inner "full programme" "/programme.html#programme") ", where talks with a " (i (@ (class "fas fa-video"))) " are available.")
     (p "Playlists with all the currently available talks are on: " ,%playlist-youtube-link ", " ,%playlist-peertube-link ".")
     ,(title-link 'h2 "Keynotes")
     ,(speaker/invited-noblurb elena)
     ,(speaker/invited-noblurb mark)
     ,(speaker/invited-noblurb omar)
     ,(speaker/invited-noblurb jelle)
     (div (@ (class "passed"))
	  ,(title-link 'h2 "Important dates (all passed)")
	  (ul
	   (li "Oral submissions closed: " ,%submission-deadline-date)
	   (li "Notification of acceptance for oral submissions: " ,%acceptance-date)
	   (li "Poster and exhibition submissions closed: " ,%registration-close-date)
	   (li "Registration closed: " ,%registration-close-date)))
     (p "Keep an eye on the "
	,(link/inner "News" "/news")
	,(link/inner '(i (@ (class "fas fa-rss-square")))
		     "/feed.xml"
		     #:attrs '((class "icon-stuck-right")))
	" to stay updated on any changes in the organisation."))))

(define call-page
  (static-page/sxml
   "Call"
   "call.html"
   `(,(title-link 'h1 "Call for Proposals")
     (p (strong "Note: the call for proposals is now closed."))
     (p "Our understanding of language is currently undergoing a major
shift as practitioners and researchers from many fields recognise the
constitutive role of embodied interactions in its emergence. Instead
of a primarily symbolic capacity, language is increasingly seen as an
activity fluidly grown from our enaction (i.e. our co-generation and
navigation) of ties and interactions with other bodies. The complexity
of language as we know it seems to be not so much the result of
individual brain-based computing capacities as an emergence from our
constant negotiation of the tensions inherent to the dynamics of
everyday interactions. Languaging, rather than language, is thus a
particular way of engaging with others and the world around us, and
pervades all levels of our action and perception.")
     (p "A strikingly parallel move is happening in the technology
world: reconnecting with research ideas lost behind the emergence of
the Personal Computer, technologists have started to break the
stranglehold of designing devices for the brain in a vat, which
effectively restricts our relationship to machines (and through them
to other humans) to minute finger movements in front of a small screen
filled with symbols. Instead, mediums are being designed to appeal to
body and mind indistinctly, developing rich manual and spatial
dynamics to facilitate interactions with people and machines, without
substituting themselves to an environment conceived as already
rich. Instead of isolating individuals into bodily inertness, such
systems are designed to support us in navigating interactions with
other people and with our own thought processes.")
     (p "These two movements share strong views about the ways in
which technological creations and scientific questions about body and
mind can be more ethical and humane, while coming from complementary
starting points. For instance, the study of languaging is concerned
with broader experimental validation, when medium design could benefit
from theoretical work to guide future design choices. The ELDM
workshop aims to bring these communities together to share views and
needs. We call for contributions concerned with embodied interactions,
languaging, and humane or interaction-centric computing mediums, and
propose a space to cross-pollinate and exchange about current
developments in academia, technology and arts.")
     ,(title-link 'h2 "General topics")
     (ul
      (li "Embodied interactions")
      (li "Languaging")
      (li "Humane dynamic medium")
      (li "Complexity of interactions")
      (li "Enactive cognitive science")
      (li "Diversity and alterity computing")
      (li "Human-computer-human interaction")
      (li "Interaction nurturing and support"))
     (div (@ (class "passed"))
	  ,(title-link 'h2 "Important dates (all passed)")
	  (p
	   "Oral submissions closed: " ,%submission-deadline-date
	   (br)
	   "Notification of acceptance for oral submissions: " ,%acceptance-date
	   (br)
	   "Poster and exhibition submissions closed: " ,%registration-close-date)
	  ,(title-link 'h2 "Guidelines for submitting (all deadlines passed)")
	  ,(title-link 'h3 "Posters or exhibitions")
	  (p "There are up to 12 poster slots available. If you are
interested in submitting but have doubts or questions, "
	     ,(mailto-sl "ask") "!")
	  (p
	   "To submit, please write to "
	   ,(mailto-sl %email-sl)
	   " with the subject line "
	   (code "ELDM2020 Submission")
	   ", and include:")
	  (ul
	   (li "an abstract of " (strong "about 300 words"))
	   (li "the type of presentation you propose (poster, exhibition, other)")
	   (li "the list of authors and affiliations if applicable"))
	  ,(title-link 'h3 "Talks or demos")
	  (p "There are up to 6 slots for contributed talks or demos,
which will be 20 minutes presentation and 10 minutes questions. If you
are interested in submitting but have doubts or questions, "
	     ,(mailto-sl "ask") "!")))))

(define (talk-page talk)
  (static-page/sxml
   (talk-title talk)
   (talk-slug talk)
   `((h2 ,(talk-authors talk))
     (h1 ,(talk-title talk))
     ,(case (talk-video-status talk)
	((#f) `())
	((public)
	 `((div (@ (class "video"))
		(div
		 (iframe (@ (sandbox "allow-same-origin allow-scripts") (allowfullscreen "")
                    (allow "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share")
			        (src ,(string-append "https://www.youtube.com/embed/" (talk-video-youtube talk)))))
		 (p (small "Watch on: "
			   ,(link "YouTube" (string-append "https://www.youtube.com/watch?v=" (talk-video-youtube talk)))
			   ", "
			   ,(link "PeerTube" (string-append "https://peertube.co.uk/videos/watch/" (talk-video-peertube talk)))))))))
	((editing)
	 `(em "Note: this talk's recording is being edited and will appear here in the coming weeks."))
	((private)
	 `(em "Note: for various reasons this talk's recording is not shared publicly — please contact the authors for any requests.")))
     (h2 "Abstract")
     ,(talk-abstract talk))
   #:copyright `("The content on this page is © 2020 " ,(talk-authors talk) ".")))

(define talk-pages
  (map
   talk-page
   (append talks-invited talks-contributed posters-contributed)))

(define (format-talk talk)
  `(span
    ,(talk-authors talk)
    ", "
    (em ,(link/inner (talk-title talk) (talk-slug talk)))))

(define (contributed-item talk)
  `(li ,(format-talk talk) "."))

(define (programme-row start end talk class)
  `(tr (@ (class ,class))
       (td (@ (class "time-start")) ,start)
       (td (@ (class "time-sep")) "—")
       (td (@ (class "time-end")) ,end)
       ,(if (talk? talk)
	    `((td (@ (colspan 1)) ,(format-talk talk))
	      ,(case (talk-video-status talk)
		 ((public)
		  `(td (@ (class "broadcast"))
		       ,(link/inner
			 `(i (@ (class "fas fa-video")))
			 (talk-slug talk)
			 #:attrs '((title "View this talk's recording")))))
		 ((editing)
		  `(td (@ (class "broadcast"))
		       (i (@ (class "fas fa-spinner")
			     (title "This talk's recording is being edited")))))
		 ((private)
		  `(td (@ (class "broadcast"))
		       (i (@ (class "fas fa-video-slash")
			     (title "This talk's recording is not shared publicly — please contact the authors for more information")))))))
  	    `(td (@ (colspan 2)) ,talk))))


(define (programme-talk start end talk)
  (programme-row start end talk "talk"))

(define (programme-break start end title)
  (programme-row start end title "break"))

(define programme-page
  (static-page/sxml
   "Programme"
   "programme.html"
   `(,(title-link 'h1 "Invited speakers")
     ,(speaker/invited elena)
     ,(speaker/invited mark)
     ,(speaker/invited omar)
     ,(speaker/invited jelle)
     ,(title-link 'h1 "Programme")
     (p "Playlists with all the currently available recordings are on: " ,%playlist-peertube-link ", " ,%playlist-youtube-link ".")
     (table (@ (class "programme"))
	    (tbody
	     ,(programme-break "8.30"  "9"     "Coffee and breakfast snacks available")
	     ,(programme-break "9"     "9.10"  "Welcome")
	     ,(programme-talk  "9.10"  "10"    (speaker-talk jelle))
	     ,(programme-talk  "10"    "10.30" talk-bruno-galantucci)
	     ,(programme-break "10.30" "10.50" "Coffee")
	     ,(programme-talk  "10.50" "11.20" talk-kris-lund)
	     ,(programme-talk  "11.20" "11.50" talk-vincenzo-raimondi)
	     ,(programme-talk  "11.50" "12.40" (speaker-talk omar))
	     ,(programme-break "12.40" "13.40" "Lunch")
	     ,(programme-talk  "13.40" "14.30" (speaker-talk elena))
	     ,(programme-break "14.30" "14.50" "Coffee")
	     ,(programme-talk  "14.50" "15.20" talk-didier-bottineau)
	     ,(programme-talk  "15.20" "15.50" talk-jasper-van-den-herik)
	     ,(programme-talk  "15.50" "16.20" talk-chris-mitchell)
	     ,(programme-break "16.20" "16.40" "Coffee")
	     ,(programme-talk  "16.40" "17.30" (speaker-talk mark))
	     ,(programme-break "17.30" "17.40" "Poster set-up")
	     ,(programme-talk  "17.40" "19"    `(,(link "Poster session" "#posters") " with food and drinks"))))
     ,(title-link 'h2 "Posters")
     (ul ,(map contributed-item posters-contributed)))
   ;; TODO: request a CC license for each accepted abstract, and add this info
   ;; TODO: add CC-BY-SA to programme
   #:copyright "On this page, the invited speaker blurbs and pictures are © the speaker (noted above each blurb), all rights reserved."
   ))

(define practicalities-page
  (static-page/sxml
   "Practicalities"
   "practicalities.html"
   `((div (@ (class "passed"))
	  ,(title-link 'h1 "Registration (now closed)")
	  (p "Registration for physical and remote presence is now closed."))
     ,(title-link 'h1 "Venue")
     (p "Room D8.001, "
	,(link "Buisson Building" "https://www.openstreetmap.org/way/44049451")
	", Descartes Campus of the "
	,(link "ENS Lyon" "http://www.ens-lyon.fr/en/campus-life/campus-tour/maps-directions")
	". Enter through the ENS Lyon main entrance, 15 parvis René Descartes, then follow the signs.")
     (div (@ (class "venue-map"))
      (iframe (@ (scrolling "no")
		 (src "https://www.openstreetmap.org/export/embed.html?bbox=4.827670454978944,45.73062149356424,4.836376905441285,45.734605541095014&layer=mapnik&marker=45.73261355285542,4.8320236802101135"))))
     (p ,(link `((img (@ (class "img-icon") (src ,(string-append %base-url "/images/osm.svg")))) "View larger map") "https://www.openstreetmap.org/?mlat=45.73261&mlon=4.83202#map=18/45.73261/4.83202"))
     ,(title-link 'h2 "Getting there")
     (p ,(link "TCL" "https://www.tcl.fr/en") " runs the public transport system in Lyon with buses, trams, and metro lines. It works well.")
     (p "If you are arriving by train:"
	(ul
	 (li "From the Gare Part-Dieu, " ,(link `("take the Metro B towards " (em "Gare d'Oullins") " and get off at " (em "Debourg")) "https://citymapper.com/trip/T7tggvq1xch"))
	 (li "From the Gare Perrache, " ,(link `("take the Tram T1 towards " (em "Debourg") " and get off at the end of the line") "https://citymapper.com/trip/Te4kou8xftm"))))
     (p "If you are arriving at the airport:"
	(ul
	 (li ,(link "Take the Rhônexpress to Gare Part-Dieu, then enter the train station to walk all the way through to the other side, and take the Metro B as explained above" "https://citymapper.com/trip/T4g8menpdfy") " — Rhônexpress tickets can be " ,(link "bought in advance" "https://www.rhonexpress.fr/en"))
	 (li "A slower but cheaper option is to " ,(link "take the TCL bus from the airport" "https://citymapper.com/trip/Tzicn13tcp9"))))
     (p "There are vending machines for TCL tickets at every metro station and nearly every tram stop, and you can buy single fares in a bus. Validate your ticket when you enter the bus, tram, or metro, after which you can use the ticket for one hour on the whole network. It's probably best to buy a bunch in one go, as you might be taking the public transport several times while in Lyon.")
     ,(title-link 'h2 "Where to stay")
     (p "If this is your first time in Lyon and you're wondering where to stay, these might interest you or provide a point of comparison (from closer to further away from the venue):")
     (ul
      (li ,(link "Appart-Hôtel Lyon Gerland" "https://www.appartcity.com/fr/destinations/rhone-alpes/lyon/lyon-gerland.html"))
      (li ,(link "B&B Hôtel Lyon Centre Perrache Berthelot" "https://www.hotel-bb.com/fr/hotel/lyon-centre-perrache-berthelot"))
      (li ,(link "Résidence Villemanzy" "http://www.belambra-villemanzy.fr/en"))
      (li ,(link "Hôtel du Théâtre" "https://www.hotel-du-theatre.fr/"))))))

(define remote-page
  (static-page/sxml
   "Remote"
   "remote.html"
   `((div (@ (class "passed"))
	  ,(title-link 'h1 "Remote participation (now closed)")
	  (p "The workshop talks and discussions will be recorded and live streamed, so people can still follow if they can't make it physically. There are several options to follow remotely.")
	  ,(title-link 'h2 "Conference call for active participation")
	  (p "A " ,(link "Jitsi Meet" "https://meet.jit.si/") " conference call including the room's live stream will open once the workshop starts. Participants to this call can ask questions during Q&A sessions, so choose this option if you wish to participate actively.")
	  (p "To participate in this call, please "
	     (strong "write to " ,(mailto-sl %email-sl) " with the subject line " (code "ELDM2020 Remote") " no later than " ,%registration-remote-date)
	     ". We will send you a link a few hours before the workshop starts.")
	  ,(title-link 'h2 "Live stream")
	  (p "The conference call itself will be streamed on the " ,(link/inner "home page" "/#live-stream") " using Youtube Live, so more casual viewers can follow both the workshop and the active remote participants.")
	  (p "Some talks are under embargo pending publication and will only be available on the conference call. The public stream will be paused during those talks, and the " ,(link/inner "Programme" "/programme.html#programme") " marks them with a " (i (@ (class "fas fa-phone"))) ".")
	  ,(title-link 'h2 "Toots and Tweets")
	  (p "Use the " (code "#eldm2020") " hashtag on "
	     ,(link "Mastodon" "https://mastodon.social/web/timelines/tag/eldm2020")
	     " and "
	     ,(link "Twitter" "https://twitter.com/hashtag/eldm2020")
	     "! If energy allows, we may also forward written questions there to the Q&A sessions.")))))

(define 404-page
  (static-page/sxml
   "Page not found"
   "404.html"
   `((h1 "Oops! That's a 404.")
     (p "You stumbled into outer space, and who knows what lies there? In any case, this is not the page you're looking for. Maybe go back to the "
        ,(link/inner "main entrance" "/")
        "?"))))

(site #:title "ELDM 2020"
      #:domain "wehlutyk.gitlab.io"
      #:default-metadata
      `((author . "Sébastien Lerique")
        (email  . ,%email-sl))
      #:readers (list commonmark-reader)
      #:builders (append talk-pages
			 (list (blog #:theme eldm-theme #:prefix "/news" #:collections %collections)
			       (atom-feed)
			       (atom-feeds-by-tag)
			       fonts-css
			       fontawesome-solid-css
			       index-page
			       call-page
			       programme-page
			       practicalities-page
			       remote-page
			       404-page
			       (static-directory "images")
			       (static-directory "css")
			       (static-directory "js")
			       (static-directory "fonts"))))
