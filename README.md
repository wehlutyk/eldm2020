# [ELDM 2020: Embodied interactions, Languaging and the Dynamic Medium](https://wehlutyk.gitlab.io/eldm2020/)

Master repository for organising the ELDM 2020 workshop, bringing together the study of Embodied interactions, Languaging, and the Dynamic Medium in February 2020.

Build what there is to build (currently the proposal): `make`

Build continuously: `make watch`

Clean up build artefacts: `make clean`
