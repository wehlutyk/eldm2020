---
title: |
  | \normalsize \textcolor{Gray}{Workshop proposal}
  | \LARGE Embodied interactions, Languaging
  | and the Dynamic Medium
subtitle: February 2020, Lyon
geometry: a4paper
bibliography: "Enactive DL.bib"
link-citations: True
colorlinks: True
header-includes:
- \usepackage{palatino}
- \newcommand{\todo}[1]{{\color{RedViolet} \#~TODO:} {\color{RedViolet}#1}}
---

\todo{

- focus on something exciting yet well-defined and doable

- add some 'being/dwelling in the world' from Ingold (e.g. "In the very objectification of the senses, as things one can have an anthropological study of, it seems that the eyes, ears and skin are no longer to be regarded as organs of a body that, as it makes its way in the world, attentively looks, listens and feels where it is going.")

- add some Experimental Semiotics: alignment emerging in interaction, identifying words and distinguishing meaningful from meaningless is hard. Also include Galantucci, Roberts, Garrod in people I write to

- add a note on Vehicles

other e.g., from Jelle: "His whole point is that granting a study of formal systems of representation the exclusive right of the only possible way to understand how culture is  enacted is getting in the way of the fact that much of what is needed to explain about cultural practices is primordial to representation, namely, it is grounded in embodied action. Only on the basis of a phenomenological understanding of embodied action can we try to make sense of symbolic representation in the first place, and the way to understand such embodied sense making is to take a fundamentally participatory approach, if we want to have any chance in finding out how representations actually work, that is, what they actually *do* in the practice"

Factor in the following:
}


## 1. Experimental buildup of the enactive approach to language

Di Paolo, Cuffari and De Jaegher [have][0] [been][1] [developing][2] an approach to language which attempts to start from a very bare bones point. Based on autopoïesis, which they extend into a notion of autonomous systems, they start by describing the tensions that appear in the interaction of two entities that are not necessarily aware of each other as entities (vs. just being a part of the surrounding environment). I.e. no pro-sociality. At that point they observe that there is a fundamental tension in any interaction, between what each participant wants to do individually, and what the dynamics of the interaction itself might impose on the participants (even though the participants' goals might be aligned). If the participants become sensitive to that tension, they can behave accordingly and navigate out of it, which in turn leads them to another level of tension. And so on and so forth: a series of sensitivities develop, and with them transformations of interactive tensions, which successively lead to the emergence of communicative acts (which they name "partial acts"; these only succeed through cooperation), regulation of partial acts by other acts, turn-taking, self-directed utterances, reporting and incorporation of other utterances, etc. The description is theoretical/philosophical, but lays down a path to understand all aspects and varieties of language while still grounding every concept in the materiality and dynamics of interaction, and in the constant work of maintaining one's identity and coherence at various scales (biological, sensorimotor, linguistic) despite the inherent precariousness of such identity.

A few Perceptual Crossing experiments illustrate the emergent behaviours they propose. The initial [Auvray et. al (2009)][3] paper illustrates the base tension of interactions (or participatory sense-making, using Di Paolo et al.'s concepts), then [Froese et al. (2014)][4] shows how turn-taking can appear through added constraints and sensitivities. Now there is still much to do to explore the existence and details of the further stages they propose, such as the distinction between spontaneous and conventionalised partial acts, the recursive regulation and the coordination of social acts, and the levels that follow. A first immediate step would be to adapt perceptual crossing experiments to a richer interaction medium that allows for different acts to appear (e.g. crossings of dots or short bars on a 2D surface, but there might be more parsimonious setups), or a medium allowing for two actions/crossings to take place at the same time (multiple cursors for instance). Following that, I see the possibility of embedding such tasks in a network of interactors in order to explore the local/portable acts tension (for a recent take on this, see the [slides of a presentation][5] made at the [Interaction and the Evolution of Linguistic Complexity][6] 2019 workshop in Edingburgh). This can all be done in the browser (which I have enough experience with), or using tools from item 2.

[0]: http://link.springer.com/article/10.1007/s11097-007-9076-9
     De Jaegher, Hanne, and Ezequiel Di Paolo. “Participatory Sense-Making.” Phenomenology and the Cognitive Sciences 6, no. 4 (December 1, 2007): 485–507. https://doi.org/10.1007/s11097-007-9076-9.
[1]: http://link.springer.com/article/10.1007/s11097-014-9404-9/fulltext.html
	 Cuffari, Elena Clare, Ezequiel Di Paolo, and Hanne De Jaegher. “From Participatory Sense-Making to Language: There and Back Again.” Phenomenology and the Cognitive Sciences 14, no. 4 (2015): 1089–1125. https://doi.org/10.1007/s11097-014-9404-9.
[2]: https://mitpress.mit.edu/books/linguistic-bodies
	 Di Paolo, Ezequiel A, Elena Clare Cuffari, and Hanne De Jaegher. Linguistic Bodies: The Continuity between Life and Language. Cambridge, MA, USA: MIT Press, 2018.
[3]: http://www.sciencedirect.com/science/article/pii/S0732118X07000748
	 Auvray, Malika, Charles Lenay, and John Stewart. “Perceptual Interactions in a Minimalist Virtual Environment.” New Ideas in Psychology 27, no. 1 (April 2009): 32–47. https://doi.org/10.1016/j.newideapsych.2007.12.002.
[4]: http://www.nature.com/srep/2014/140114/srep03672/full/srep03672.html
	 Froese, Tom, Hiroyuki Iizuka, and Takashi Ikegami. “Embodied Social Interaction Constitutes Social Cognition in Pairs of Humans: A Minimalist Virtual Reality Experiment.” Scientific Reports 4 (January 14, 2014): 3672. https://doi.org/10.1038/srep03672.
[5]: https://wehlutyk.gitlab.io/ielc2019-presentation/#/start
	 Lerique, Sébastien, Dan Dediu, Márton Karsai, and Jean-Philippe Magué. “Individual variation, network heterogeneity and linguistic complexity: which way does the relationship go?”. IELC 2019 Contributed talk.
[6]: http://www.lel.ed.ac.uk/cle/index.php/ielc2019/


## 2. Develop tools to realise the vision of "diversity computing"

[Diversity computing][7] is a new approach to HCI and computing, which transposes much of the ideas of the enactive approach to technology. Basically, it observes that the collective, inter-individual, "groupal", interactive dimension of life is left behind in most contemporary computing, HCI, and forward-thinking in general around "the digital" (this is also true in the legal domain, as shown by Alain Supiot for instance). It suggests that instead of creating devices that are targeted for individuals (and targeted for creating addiction, though that is another matter, more at the application level), we should build computing tools with the fundamental tensions of interaction in mind, systems for "helping interaction", supporting tie-making, making dissent manageable, allowing people to "dilute" more and maybe let themselves get closer to precariousness/vulnerability at times. Systems that are interaction-centric, not content-centric, that would allow us to interact without getting lost in the symbolic world, maintaining our engagement with the concrete world and bodies/people around us. The moonshot aim here is to invent interaction mediums (or support systems) that may support our current language (or provide a new one), where we have explored the set of constraints, type of support, and difficulties that such a system brings to the interaction. Mediums or places that support the growth of new modes of interpersonal understanding.

If this sounds outlandish you can ignore it, as the exact role that technology can play here is still much too vague to be at the core of a proposal (and maybe even to mention this item at all, since at first sight it could be tackled using practices like non-violent communication).

But this long-term item motivates me a lot, and I think tools coming from here can be directly useful to item 1. Specifically, I would like to build a physical space inspired by [Dynamic Land][8], where immersive perceptual crossing experiments and sensory substitution experiments are easy and quick to implement. Dynamic Land is a place led by Bret Victor, whose ideas are best seen in his presentations: [The Humane Representation of Thought][9] (1 hour video), or [A brief rant of the future of interaction design][10] (a short-medium essay).


**TL;DR for Bret Victor & Dynamic Land**

His starting point is that current computing technology restricts our interaction to moving our fingers in front of a small rectangle (the screen), which is terribly wasteful of the rich capabilities we have to interact with our full body (he takes the example of the complexity of things we do with our hands when we prepare breakfast; or moving around in a room or museum to exploit spatial understanding, etc.). So today, "intellectual work" requires sitting at a desk in front of a computer, and constrains the range of experiences and modes of understanding we use for that work; but it could be much richer and much more bodily if we had a richer medium than today (i.e. text on screen).

The proposed solution is to develop the "dynamic medium", which is to sculpture what screen is to paper. Sort of a malleable/programmable/structurable material that we can interact with in structured ways, e.g. programming it in real time through an interaction which does not involve typing text on a keyboard, but rather moving hands and feeling forces and textures in some 3D material with enough ease and speed that the system can support speech.

Dynamic Land, their first working system, adds a layer of computer-capability to physical objects in a room (currently only sheets of paper), using a setup of cameras and video-projectors in the ceiling. For instance, sheets of paper on a table have some code associated to them, are aware of their surroundings, and can send messages to those surroundings. It's extremely easy to program the system, as pointing a keyboard to a sheet of paper shows you the associated code and lets you edit it directly (no need for screens). The system is collaborative at its core, does away with the scarcity of screens (since the medium is sheets of paper), pushes people to interact, and the richness of the relative positioning of papers and small objects on the table creates a very direct (yet rich) layer of interaction with a system capable of computation, without having to program.

It's best understood by looking at the videos on their home page. Their references for similar technological changes in the past are the invention of mathematical notation (which made describing mathematics much easier than in words), and the invention of graphical plots (before which numbers were only represented in tables), which made it possible to visually understand numbers. (A strong inspiration is Seymour Papert's [Mindstorms][11], itself relying much on Piaget for the idea that, given the right environment in which you learn or communicate, some things can become completely obvious to understand.)

**End of TL;DR**


A space similar to this (for which I've started thinking about implementation details), designed with a focus on "dynamics of interaction", can be an extremely rich environment for the exploration of immersive interactions that have different dynamics than what we know today. I see it as a general interaction-dynamics creation tool where we can test different types of interactions and show how specific meanings, thoughts, or relevant patterns of interaction are structured by the choice of dynamics, or how some dynamics are better suited to some patterns and meanings than other interaction dynamics (these are example applications, there could be many more). It is sort of an "in vivo" version of the Perceptual Crossing experiments (which can be seen as "in vitro"), where interactors can share much more than just "crossing each other", and more varied types of partial acts can arise.

Again, this might not appear in the proposal (or only very briefly), but it's a direction I would like to explore in the long term, and a type of tool that might be useful in the short term.

[7]: http://doi.acm.org/10.1145/3243461
	 Fletcher-Watson, Sue, Hanne De Jaegher, Jelle van Dijk, Christopher Frauenberger, Maurice Magnée, and Juan Ye. “Diversity Computing.” Interactions 25, no. 5 (August 2018): 28–33. https://doi.org/10.1145/3243461.
[8]: https://dynamicland.org/
[9]: https://vimeo.com/115154289
[10]: http://worrydream.com/ABriefRantOnTheFutureOfInteractionDesign/
[11]: https://www.worldcat.org/title/mindstorms-children-computers-and-powerful-ideas/oclc/794964988
	  Papert, Seymour. Mindstorms: Children, Computers, and Powerful Ideas. New York: Basic Books, 1980.

# Introduction

Our understanding of language is currently undergoing a major shift
as practitioners and researchers from many fields
recognise the constitutive role of embodied interactions in its emergence.
Instead of a primarily symbolic capacity, language is increasingly seen as an activity fluidly grown from our enaction (i.e. our co-generation and navigation) of ties and interactions with other bodies.
The complexity of language as we know it seems to be not so much the result of individual brain-based computing capacities as an emergence from our constant negotiation of the tensions inherent to the dynamics of everyday interactions.
*Languaging*, rather than language, is thus a particular way of engaging with others and the world around us, and pervades all levels of our action and perception.

A strikingly parallel move is happening in the technology world:
reconnecting with research ideas lost behind the emergence of the Personal Computer,
technologists have started to break the stranglehold of
designing devices for the brain in a vat,
which effectively restricts our relationship to machines (and through them to other humans)
to minute finger movements in front of a small screen filled with symbols.
Instead, mediums are being designed to
appeal to body and mind indistinctly, developing rich manual and spatial dynamics
to facilitate interactions with people and machines without substituting themselves to an environment conceived as already rich.
Instead of isolating individuals into bodily inertness, such systems are designed to support us in navigating interactions with other people and with our own thought processes.

These two movements share strong views about the ways in which technological creations and scientific questions about body and mind can be more ethical and humane, views which the ELDM workshop aims to bring together.
We call for contributions concerned with embodied interactions, languaging, and humane or interaction-centric computing mediums, proposing a space to cross-pollinate and exchange about current developments in academia, technology and arts.

**Keywords**

- Embodied interactions
- Languaging
- Humane dynamic medium
- Complexity of interactions
- Enactive cognitive science
- Diversity computing
- Human-computer-human interaction

# Scientific context

Our understanding of language, along with its emergence and its evolution, is currently undergoing major theoretical and experimental shifts.

Since the middle of the 20th century, mainstream approaches to explaining language were strongly influenced by the computational metaphor of mind:
as cognition was mainly seen as information processing of sensory inputs for continuous inference, language could be seen as the manifestation of a set of formal symbolic and logical structures, with corresponding hardware to process such structures in the brain. Relevance Theory, for instance, relies on a hypothesized logic-processing “device” in the brain (e.g. a brain area or nucleus) capable of rapid inference of the relevant logical entailments of an utterance in a given context. Approaches in this area share a common core object of study: the individual, and its capacity for inference based on information provided by a given context.

Towards the end of the 20th century however, the computational metaphor has been increasingly challenged:
while the brain undoubtedly implements many information processing mechanisms,
taking such capacities to be the core of cognition has maintained a misleading separation between mind and body and between self and others, leading to an impoverished view of both sides of these dichotomies, and dead-ends concerning language and the study of interactional context.

Together with Cognitive Linguistics, the so-called “4E”^[Embedded, extended, embodied and enactive approaches.] approaches have reconnected the study of language with the reality of the body and the interactive situations in constantly evolves with.
The Enactive approach, in particular, extends work spearheaded by @varela_embodied_1991 to develop an explanation of language and meaning entirely grounded in the materiality and dynamics of interactions [@di_paolo_linguistic_2018]. By starting from the dynamics of interacting pairs, this theory identifies a fundamental tension that exists between individual-level goals and the higher-order dynamics of the interaction taken as an emergent system [@de_jaegher_participatory_2007]. Having to manage such tensions, and eventually transforming them as participants become sensitive to such higher-order dynamics, generates a constant pressure for the emergence of structure in interactions. This pressure paves the way for the gradual emergence of linguistic patterns in interactions [@di_paolo_linguistic_2018]. This theoretical move is paralleled by the growth of experiments in Perceptual Crossing [@froese_embodied_2014], Iterated Learning [@carr_cultural_2017] and Experimental Semiotics [@roberts_investigating_2017], which look for the emergence of structure in communication mechanisms due to interaction dynamics and the accumulation of individual biases.

A similar shift is happening in technology and the field of human-computer interaction, as a growing number of technologists agree that current computing systems are overly individual-based. Combined with the restricted interactions that screen-based interfaces afford [@victor_humane_2014], these tend to isolate people from one another instead of supporting interactions and helping them deal with alterity [@fletcher-watson_diversity_2018]. In reaction, group-level interaction-centric systems with rich tangible interfaces are being designed [@victor_dynamicland_2017] with the aim to support collaborative interaction with diverse participants, and explore their emergent dynamics. The parallel between such interaction-first, diversity- and group-aware systems, and the move to theories of language based on the dynamics of interaction is striking, so much that cross-pollination and collaborations should be fruitful.

# Relationship to complex systems

\todo{factor this out}

The Enactive approach to the study of language is inherently transdisciplinary, as it proposes a way to understand how social norms, institutions, community conventions, interaction patterns, linguistic patterns (including language processing and production), and individual cognitive capacities generate one another and are at the same time visible in bodies. It relies on the analysis of dynamical systems to understand dyadic interaction patterns, on human-computer interfaces and body movement measures for creating and analysing minimal interaction environments, on the study of complexity to analyse the emergence and sedimentation of patterns and conventions in networks of interacting people, and on social anthropology for understanding the significance of the diversity that emerges in, and is supported by, these processes.

This workshop aims to bring together communities that, while currently disconnected, share an interest for the complexity that emerges through the dynamics of interaction, as well as its effects at large scale (such as the emergence of language, the processes of mutual understanding that can be supported by computer systems, or the diversity of the network of people which such emergent processes rely on). The goal is precisely to create links between disciplines for the study of this common object.

# Proposal

The developments mentioned above testify to the growth of approaches that take into account (i) the multi-scale aspect of human dynamics, (ii) the interactions across different scales, and corresponding tensions between scales, and (iii) the diversity and possible tensions between entities inside each scale; each of these points is taken to be a core ingredient in the dynamics of human life, necessary to consider seriously in order to understand or act on these dynamics. This resonates with the study of emergent effects in complex systems, but also with the retroaction that such emergent entities might have over lower-level entities (in the case of social norms: individual actions coordinate and evolve into sedimented norms which become the substrate for further actions, from which individuals might then want to depart). Indeed, at their core, the study of language and the development of natively interactive and collaborative computing systems are a flavour of systems thinking focused on systems complexity. Importantly, however, they come from complementary concrete problems, and use complementary tools to attack those problems.

Diversity computing [@fletcher-watson_diversity_2018] and rich interaction-first computing systems [@victor_humane_2014;@victor_dynamicland_2017] are in the business of exploring how computing can be rethought from the ground up in order to support collective processes (such as interactive exploration of a system, co-creation, or simply debating a controversy). While having a strong focus on cognitive capacities is an important part of any human-computer interaction work, the novel aspect of this approach is the recognition of points (i) and (iii) above. As such, it aims to create tools that widen each participant's sensitivity to the diversity of behaviours, cognitive styles, and backgrounds that participants come from, in order to help manage the tensions and navigate towards synergies instead of falling into a state of isolated activity for each person in the group. Aside from technical development, its current need is for solid theories that can guide future concrete design choices. The Enactive approach to language lives in a different context: it aims to further the conceptual shift initiated in cognitive science since the 2000s, which it does by fully cashing out the theoretical consequences of taking points (i) and (ii) above seriously in a theory of cognition in groups. Its current need is for exhaustive experimental validation using controlled minimal interaction experiments [such as extensions of @froese_embodied_2014], as well as experiments that allow for gradually more complex behaviours to emerge and be studied, eventually reaching experiments where the interaction between participants starts acquiring important features of the protolanguages predicted in theory.

The two fields could therefore greatly benefit from cross-pollination of goals and tools. The Enactive approach is evolving into a mature theoretical base that can inspire and guide the design processes of diversity- and interaction-centric computing systems. Conversely, such computing systems, if adapted properly, could become a core component of experimental setups aiming to validate and further understand the emergence of linguistic complexity through interactions. Indeed, an all-encompassing interactive computing environment would allow researchers to create rich mediums (or substrates) for interaction, sufficiently close to reality to allow for complex behaviours to emerge and be studied, while maintaining a selective control over the flow of information and the type of communication between participants.

Several examples of this type of tool already exist. The Enactive Torch [@lobo_route_2019;@creativerobotics_enactive_2019], for instance, is a hackable device used to experimentally test the predictions made by interaction-based theories of cognition (such as the 4Es). Perceptual Crossing experiments [@froese_embodied_2014] rely on computer interfaces such as Tactos [@auvray_perceptual_2009] to create an interaction setting with a minimal communication channel. Interaction-centric computing environments which are fully and easily adaptable such as [@victor_dynamicland_2017] can already be used to implement simple experimental settings to validate hypotheses on the emergence of complexity through interactions.
The proposal we put forth is to organise a one to two days workshop bringing together major actors in these two communities. A first goal is to create an initial venue for the two communities to get to know each other, discover the context, problematics faced, and contributions made by each actor. A second goal is to support cross-pollination of ideas, tools and goals, opening the field for collaborations; moving forward, we aim to create the conditions to federate some concerns in the two communities, by identifying entry points for moving forward both in the awareness that interaction-centric computer systems can have of the inherent tensions in interactions, and in the experimental validation of the theoretical predictions of the emergence of complexity through interactions.

# References
