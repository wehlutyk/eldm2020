FROM archlinux/base

RUN pacman --noconfirm -Syu
RUN pacman --noconfirm -S base-devel git gperf

# Prepare build directory so as not to build as root
RUN mkdir /home/build
RUN chgrp nobody /home/build
RUN chmod g+ws /home/build
RUN setfacl -m u::rwx,g::rwx /home/build
RUN setfacl -d --set u::rwx,g::rwx,o::- /home/build

# Get packages
WORKDIR /home/build
RUN git clone https://aur.archlinux.org/guile-reader.git
RUN git clone https://aur.archlinux.org/guile-commonmark.git
RUN git clone https://aur.archlinux.org/haunt.git
RUN chmod -R g+w /home/build

# Install guile-reader
WORKDIR /home/build/guile-reader
RUN sudo -u nobody makepkg
RUN pacman --noconfirm -U $(sudo -u nobody makepkg --packagelist)

# Install guile-commonmark
WORKDIR /home/build/guile-commonmark
RUN sudo -u nobody makepkg
RUN pacman --noconfirm -U $(sudo -u nobody makepkg --packagelist)

# Install haunt
WORKDIR /home/build/haunt
RUN sudo -u nobody makepkg
RUN pacman --noconfirm -U $(sudo -u nobody makepkg --packagelist)

# Clean up
WORKDIR /
RUN pacman --noconfirm -Rs gperf
RUN pacman --noconfirm -Sc
RUN rm -rf /home/build
